package com.techuniversity.demospringbootinitz.controllers;

import org.springframework.web.bind.annotation.GetMapping; 
import org.springframework.web.bind.annotation.RequestParam; 
import org.springframework.web.bind.annotation.RestController;

@RestController public class HelloController {
    @GetMapping("/saludos")
    public String saludar(@RequestParam(value="nombre", defaultValue="Tech U!")
                                        String name){
        return String.format("Hola %s ;)", name);
  }
} 