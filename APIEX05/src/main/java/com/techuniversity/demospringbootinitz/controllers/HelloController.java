package com.techuniversity.demospringbootinitz.controllers;

import org.springframework.web.bind.annotation.GetMapping; 
import org.springframework.web.bind.annotation.RequestParam; 
import org.springframework.web.bind.annotation.RestController;

@RestController public class HelloController {
  @GetMapping("/productos/{id}")
  public Producto getProductoId(@PathVariable Integer id) {
   return dataService.getProducto(id);
  }
} 