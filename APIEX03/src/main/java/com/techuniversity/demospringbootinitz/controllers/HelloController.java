package com.techuniversity.demospringbootinitz.controllers;

import org.springframework.web.bind.annotation.*;  



@RestController
public class HelloController {

	@RequestMapping("/HELLO")
	public String index() {
		return "Greetings from Spring Boot!";
	}

}


