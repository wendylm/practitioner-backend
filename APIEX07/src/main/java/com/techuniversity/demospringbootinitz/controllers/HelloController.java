package com.techuniversity.demospringbootinitz.controllers;

import org.springframework.web.bind.annotation.GetMapping; 
import org.springframework.web.bind.annotation.RequestParam; 
import org.springframework.web.bind.annotation.RestController;

@RestController public class HelloController {
 
      @PutMapping("/productos/{id}")
      public Producto updateProducto(@PathVariable Integer id, @RequestBody Producto producto) {
        return dataService.updateProducto(id, producto);
      }
}