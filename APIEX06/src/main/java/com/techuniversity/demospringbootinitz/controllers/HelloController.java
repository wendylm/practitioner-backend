package com.techuniversity.demospringbootinitz.controllers;

import org.springframework.web.bind.annotation.GetMapping; 
import org.springframework.web.bind.annotation.RequestParam; 
import org.springframework.web.bind.annotation.RestController;

@RestController public class HelloController {
 
@PostMapping("/productos")
public Producto addProducto(@RequestBody Producto producto) {
      return dataService.addProducto(producto);
}